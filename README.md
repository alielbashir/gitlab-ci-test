# Gitlab CI/CD Test
A repository to test Github Actions

## Installation

1. Clone the repository
2. Compile server.c and client.c into Server and Client
3. Run test.py

## Usage

1. Add a test to test.py
2. Push to Main
3. Check the testing job in the repository's CI/CD pipelines tab

If all the tests in test.py pass, then the pipeline will pass successfully, otherwise the pipeline will fail.
